from django.shortcuts import render, redirect
from django.views import View
from main.models import Manager, Developer, Project


# Create your views here.
class Home(View):
    def get(self, request):
        return render(request, 'main/home.html')

    def post(self, request):
        pass


class Login(View):
    def get(self, request):
        if request.session.get('username'):
            return redirect('user')

        return render(request, 'main/login.html')

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']

        managers = Manager.objects.filter(username__exact=username)

        if managers.count() == 0 or managers[0].password != password:
            return render(request, 'main/login.html', {'error_message': 'username/password incorrect'})

        request.session["username"] = username
        return redirect('user')


class UserView(View):
    def get(self, request):
        if request.session.get('username'):
            return render(request, 'main/user.html')
        return redirect('login')

    def post(self, request):
        pass


class Logout(View):
    def get(self, request):
        request.session.pop("username", None)
        return redirect("login")


class SignUpView(View):
    def get(self, request):
        return render(request, 'main/signup.html')

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]

        if len(username) > 0 and len(password) > 0:
            all_managers = Manager.objects.filter(username__exact=username)

            if all_managers.count() != 0:
                error_message = f"{username} exists, please use another username"
                return render(request, 'main/signup.html', {"error_message": error_message})

            Manager.objects.create(username=username, password=password)
            return redirect('login')
        else:
            return render(request, 'main/signup.html', {"error_message": 'username/password not provided'})


class DeveloperView(View):
    def get(self, request):
        if request.session.get('username'):
            return render(request, 'main/developer.html', {"developers": Developer.objects.all()})
        else:
            return render(request, 'main/error.html')

    def post(self, request):
        username = request.POST["developer_username"]
        if len(username) > 0:
            if request.session.get('username'):
                manager_username = request.session.get('username')
                manager = Manager.objects.get(username__exact=manager_username)

                all_developers = Developer.objects.filter(username__exact=username)

                if all_developers.count() != 0:
                    error_message = f"developer: {username} exists, please use another username"
                    return render(request, 'main/developer.html', {"error_message": error_message,
                                                                   "developers": Developer.objects.all()})

                Developer.objects.create(username=username, project_manager=manager)
                return redirect('developer')
            else:
                return render(request, 'main/developer.html', {"error_message": 'manager is not logged in',
                                                               "developers": Developer.objects.all()})
        else:
            return render(request, 'main/developer.html', {"error_message": 'username not provided',
                                                           "developers": Developer.objects.all()
                                                           })


class ProjectCreateView(View):
    def get(self, request):
        if request.session.get('username'):
            return render(request, 'main/createProject.html', {"projects": Project.objects.all(),
                                                               "developers": Developer.objects.all()
                                                               })
        else:
            return render(request, 'main/error.html')

    def post(self, request):
        if request.session.get('username'):
            manager_username = request.session.get('username')
            project_name = request.POST['project_name']
            project_description = request.POST['project_description']
            project_priority = request.POST['project_priority']
            project_developers = request.POST.getlist('project_developers')

            manager = Manager.objects.get(username__exact=manager_username)
            if len(project_name) > 0 and len(project_description) > 0 and len(project_priority) and len(
                    project_developers) > 0:
                project = Project.objects.create(name=project_name,
                                                 description=project_description,
                                                 priority=project_priority,
                                                 project_manager=manager)

                for developer in project_developers:
                    developer_instance = Developer.objects.get(username__exact=developer)
                    project.developer.add(developer_instance)

                return redirect('view_project')
            else:
                return render(request, 'main/createProject.html',
                              {"error_message": 'project creation details not enough',
                               "developers": Developer.objects.all()})
        else:
            return render(request, 'main/createProject.html', {"error_message": 'manager is not logged in',
                                                               "developers": Developer.objects.all()
                                                               })


class ProjectDetailView(View):
    def get(self, request):
        if request.session.get('username'):
            return render(request, 'main/viewProject.html', {"projects": Project.objects.all()})
        else:
            return render(request, 'main/error.html')
